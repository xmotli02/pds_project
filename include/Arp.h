/**
 * Class for arp packet
 *
 * Author: Matus Motlik, xmotli02@stud.fit.vutbr.cz
 * 
 */

#ifndef ARP_H
#define ARP_H

#include <iostream>
#include <sstream>
#include <iomanip>

#include <string.h> 
#include <sys/types.h> 
#include <linux/if_ether.h> 
#include <arpa/inet.h>
#include <netinet/ip.h>       // IP_MAXPACKET (which is 65535)

#include "Packet.h"

using namespace std;

class Arp: public Packet {
	private:
		struct ArpHeader {
			uint16_t htype;
			uint16_t ptype;
			uint8_t hlen;
			uint8_t plen;
			uint16_t opCode;
			uint8_t sourceMac[6];
			uint8_t sourceIp[4];
			uint8_t destMac[6];
			uint8_t destIp[4];
		};
		
		ArpHeader _arpHeader;

	public:
		static const uint16_t REQUEST;
		static const uint16_t REPLY;
		static const int ARP_HDR_L;

		Arp();
		Arp(uint32_t destIp, uint8_t* destMac, 
				uint32_t sourceIp, uint8_t* sourceMac, 
				uint16_t operation);

		uint8_t* getBuffer();
		int getBufferSize();
		void setBufferSize(int size);
		void setData(uint8_t* buffer);
		uint16_t getType();
		string getSourceIp();
		string getSourceMac();

		string getDestinationIp();
};

#endif