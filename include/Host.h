/**
 * Host class reprezenting single host in local network
 *
 * Author: Matus Motlik, xmotli02@stud.fit.vutbr.cz
 * 
 */

#ifndef HOST_H
#define HOST_H

#include <iostream>
#include <cstddef>
#include <string> 
#include <vector>
#include <algorithm>

using namespace std;

class Host {
	private:
		string _macAddress;
		string _ipAddress;
		vector<string> _ipv4;
		vector<string> _ipv6;

	public:
		Host();
		Host(const string& macAddress);

		bool operator == (const Host &host);

		string getMacAddress();
		vector<uint8_t> getMacAsVector();
		void setMacAddress(const string& mac);

		string getIpAddress();
		void setIpAddress(const string& ip);

		vector<string> getIpv4Addresses();
		void addIpv4(const string& ipv4);

		vector<string> getIpv6Addresses();
		void addIpv6(const string& ipv6);

		bool isAddressOfHost(string ip);
};

#endif