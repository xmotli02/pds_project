/**
 * Intercept singleton class used for intercept 
 *
 * Author: Matus Motlik, xmotli02@stud.fit.vutbr.cz
 */

#ifndef INTERCEPT_H
#define INTERCEPT_H

#include <iostream>
#include <cstddef>
#include <string> 
#include <memory>
#include <locale>
#include <chrono>
#include <thread>

#include <linux/if_ether.h>

#include "Interface.h"
#include "Host.h"
#include "Packet.h"
#include "Ipv4.h"
#include "Ipv6.h"
#include "XmlFile.h"
#include "VictimPair.h"

using namespace std;

class Intercept {
	private:
	  string _fileName;
		shared_ptr<Interface> _interface;	
		vector<VictimPair> _victimPairs;

	  Intercept() {} 

	public:
		static Intercept& getInstance() {
			static Intercept instance;
			return instance;
    }

    Intercept(Intercept const&)      = delete;
    void operator=(Intercept const&) = delete;

    shared_ptr<Interface> getInterface();
    void setInterface(const string& interfaceName);
    string getFileName();
    void setFileName(const string& fileName);
    vector<VictimPair> getVictimPairs();
    void setVictimPairs(vector<VictimPair> victimPairs);

    void interceptOnInterface();
    void gotPacket(uint8_t* buffer, int bufferSize);
    bool isPacketFromVictimPair(uint8_t* buffer, Packet *packet);
};

#endif