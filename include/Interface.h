/**
 * Interface class reprezenting interface and used for communication
 *
 * Author: Matus Motlik, xmotli02@stud.fit.vutbr.cz
 * 
 */

#ifndef INTERFACE_H
#define INTERFACE_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <string> 
#include <vector>
#include <memory>
#include <functional>

#include <sys/types.h>
#include <ifaddrs.h>
#include <netinet/in.h> 
#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <net/if.h>
#include <string.h>
#include <unistd.h> 
#include <sys/ioctl.h>
#include <netinet/ip.h>

#include "Packet.h"

using namespace std;

class Interface {
	private:
		string _interfaceName;	  
		int _socket;
		
		uint32_t _ipv4Address;
		uint8_t _macAddress[6];
		uint32_t _ipv4Mask;

		uint8_t _ipv6Address[16];
		string _ipv6Mask;

		void _getIpAddresses();
		void _getMacAddress();

	public:
		Interface(const string& name);

		string getName();

		uint32_t getIpv4Address();
		uint8_t* getMacAddress();
		uint32_t getIpv4Mask();
		int getTotalIpv4Hosts();
		uint32_t getFirstHostIpv4();

		uint8_t* getIpv6Address();
		string getIpv6Mask();

		uint32_t convertStringIpv4(string ipv4);
		vector<uint8_t> convertStringIpv6(string ipv6);

		void createSocket();
		int getSocket();
		void closeSocket();
		void sendBuffer(uint8_t* buffer, int bufferSize);
		void sendPacket(Packet* packet);
		bool waitForReply(Packet* packet);

		void sniffOnInterface(function<void(uint8_t*, int)> callback);
};

#endif