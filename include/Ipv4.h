/**
 * Class for Ipv6 packet
 *
 * Author: Matus Motlik, xmotli02@stud.fit.vutbr.cz
 * 
 */

#ifndef IPV4_H
#define IPV4_H

#include <iostream>
#include <sstream>
#include <iomanip>

#include <string.h> 
#include <sys/types.h> 
#include <linux/if_ether.h> 
#include <arpa/inet.h>
#include <netinet/ip.h>				// IP_MAXPACKET (which is 65535)T

#include "Packet.h"

using namespace std;

class Ipv4: public Packet {
	private:
		struct ip _ipv4Header;
		uint8_t srcMac[6];

	public:
		Ipv4();
		
		uint8_t* getBuffer();
		int getBufferSize();
		void setBufferSize(int size);
		void setData(uint8_t* buffer);
		uint16_t getType();

		string getSourceIp();
		string getSourceMac();

		string getDestinationIp();
};

#endif