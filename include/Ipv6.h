/**
 * Class for Ipv6 packet
 *
 * Author: Matus Motlik, xmotli02@stud.fit.vutbr.cz
 * 
 */

#ifndef IPV6_H
#define IPV6_H

#include <iostream>
#include <sstream>
#include <iomanip>

#include <string.h> 
#include <sys/types.h> 
#include <linux/if_ether.h> 
#include <arpa/inet.h>
#include <netinet/ip.h>				// IP_MAXPACKET (which is 65535)
#include <netinet/ip6.h>			// struct ip6_hdr
#include <netinet/icmp6.h>		// struct icmp6_hdr and ICMP6_ECHO_REQUEST

#include "Packet.h"

using namespace std;

class Ipv6: public Packet {
	private:
		// Source: http://www.cs.cmu.edu/afs/cs/academic/class/15213-f00/unpv12e/icmpd/icmp6.h
		struct ip6_hdr _ipv6Header;
		struct icmp6_hdr _icmp6Header;
		struct in6_addr _icmp6Target;
		struct nd_opt_hdr _icmp6Options;
		uint8_t _icmp6MacAddr[6];

		uint8_t srcMac[6];
		uint8_t dstMac[6];

		void _extendBufferSize(int size);
		void _copyToBuffer(uint8_t* buffer, void* data, int dataLength);
		uint8_t _getIcmpType(); 
		void _setIcmp6Type(uint8_t type);
		uint16_t _getIcmpHeaderLenght();
		void _setIPv6Header(uint8_t* srcIpv6, uint8_t* dstIpv6);
		void _setIcmp6Header(uint8_t* dstIp, uint8_t* dstMac, uint8_t type);
		void _setIcmp6Target(uint8_t* target);
		void _setIcmp6Options(uint8_t* mac);
		void _copyEtherFrameToBuffer(uint8_t* etherFrame, 
							uint8_t* srcMac, uint8_t* dstMac, uint16_t type);

		uint16_t _icmp6Checksum();
		uint16_t _checksum(uint16_t* buffer, int lenght);

	public:
		static const uint8_t REQUEST;
		static const uint8_t REPLY;
		static const uint8_t ADVERTISEMENT;
		static const int IPV6_HDR_L;
		static const int ICMP6_HDR_L;
		static const int ICMP6_TARGET_L;
		static const int ICMP6_OPTIONS_L;

		Ipv6();
		Ipv6(uint8_t* dstIp, uint8_t* dstMac, uint8_t* srcIp, uint8_t* srcMac, uint8_t operation);

		uint8_t* getBuffer();
		int getBufferSize();
		void setBufferSize(int size);
		void setData(uint8_t* buffer);
		uint16_t getType();
		string getSourceIp();
		string getSourceMac();

		string getDestinationIp();
};

#endif