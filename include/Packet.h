/**
 * Interface for packets
 *
 * Author: Matus Motlik, xmotli02@stud.fit.vutbr.cz
 */

#ifndef PACKET_H
#define PACKET_H

#include <iostream>
#include <string> 

using namespace std;

class Packet {
	protected:
		uint16_t _type;
		int _bufferSize;
	public:
		static const int ETHER_HDR_L = 14;
		virtual uint8_t* getBuffer() = 0;
		virtual int getBufferSize() = 0;
		virtual void setBufferSize(int size) = 0;
		virtual void setData(uint8_t* buffer) = 0;
		virtual uint16_t getType() = 0;

		virtual string getSourceIp() = 0;
		virtual string getSourceMac() = 0;

		virtual string getDestinationIp() = 0;
};

#endif