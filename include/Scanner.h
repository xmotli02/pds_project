/**
 * Scanner singleton class used for scanning local network
 *
 * Author: Matus Motlik, xmotli02@stud.fit.vutbr.cz
 */

#ifndef SCANNER_H
#define SCANNER_H

#include <iostream>
#include <cstddef>
#include <string> 
#include <vector>
#include <algorithm> 

#include "XmlFile.h"
#include "Host.h"
#include "Interface.h"
#include "Packet.h"
#include "Arp.h"
#include "Ipv6.h"

using namespace std;

class Scanner {
	private:
	  string _fileName;
	  vector<Host> _hosts;
	
		Interface *_interface;	  

	  Scanner() {} 

	public:
		static Scanner& getInstance() {
			static Scanner instance;
			return instance;
    }

    Scanner(Scanner const&)         = delete;
    void operator=(Scanner const&)  = delete;

		string getFileName();
    void setFileName(const string& fileName);

    Interface *getInterface();
    void setInterface(const string& interfaceName);

    vector<Host> getHosts();
    Host& findOrNewHost(string macAddress);

	  void scanNetwork();

	  void createXmlFile();
};

#endif