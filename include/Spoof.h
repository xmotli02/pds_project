/**
 * Spoof singleton class used for spoofing 
 *
 * Author: Matus Motlik, xmotli02@stud.fit.vutbr.cz
 */

#ifndef SPOOF_H
#define SPOOF_H

#include <iostream>
#include <cstddef>
#include <string> 
#include <memory>
#include <locale>
#include <chrono>
#include <thread>

#include "Interface.h"
#include "Host.h"
#include "Packet.h"
#include "Arp.h"
#include "Ipv6.h"

using namespace std;

class Spoof {
	private:
		int _timeout;
		char _protocol;
		shared_ptr<Interface> _interface;	  
		shared_ptr<Host> _firstVictim;
		shared_ptr<Host> _secondVictim;

	  Spoof() {} 
	  vector<uint8_t> _macStringToVector(string macAddress);
	  void _sendPacketsInLoop(Packet* p1, Packet* p2);

	public:
		static const int ARP_P;
		static const int NDP_P;
		static Spoof& getInstance() {
			static Spoof instance;
			return instance;
    }

    Spoof(Spoof const&)         	= delete;
    void operator=(Spoof const&)  = delete;

    int getTimeout();
    void setTimeout(int timeout);
    char getProtocol();
    void setProtocol(const string& protocol);
    shared_ptr<Interface> getInterface();
    void setInterface(const string& interfaceName);
    shared_ptr<Host> getVictim1();
    void setVictim1(shared_ptr<Host> victim);
    shared_ptr<Host> getVictim2();
    void setVictim2(shared_ptr<Host> victim);

	  void spoofVictims();
	  void resetVictimsCaches();
};

#endif