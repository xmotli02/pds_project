/**
 * Class representing victim pair
 *
 * Author: Matus Motlik, xmotli02@stud.fit.vutbr.cz
 */

#ifndef VICTIM_PAIR_H
#define VICTIM_PAIR_H

#include <iostream>
#include <string> 
#include <memory>
 
#include <memory.h>

#include "Host.h"

using namespace std;

class VictimPair {
    
private:
    string _name;
    shared_ptr<Host> _firstVictim;
    shared_ptr<Host> _secondVictim;

public:
    VictimPair();
    VictimPair(const string& name);

    bool operator == (const VictimPair &pair);

    string getName();
    void setName(const string& name);
    shared_ptr<Host> getVictim1();
    void setVictim1(shared_ptr<Host> victim);
    shared_ptr<Host> getVictim2();
    void setVictim2(shared_ptr<Host> victim);

    void insertVictim(shared_ptr<Host> victim);

    bool isInVictimPair(string srcMac, string dstIp, uint8_t* dstMac); 
};

#endif