/**
 * XmlFile class used for creating xml files
 *
 * Author: Matus Motlik, xmotli02@stud.fit.vutbr.cz
 * 
 */

#ifndef XML_FILE_H
#define XML_FILE_H

#include <iostream>
#include <fstream>
#include <string> 
#include <algorithm>
#include <vector>

#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>
#include <libxml/tree.h>

#include "VictimPair.h"

#ifndef XMLCheckResult
	#define XMLCheckResult(a_eResult, a_eMessage) 								\
 		if (a_eResult != XML_SUCCESS) { 														\
	 		std::cerr << "Xml: " << a_eMessage << " - Error: " 				\
	 							<< a_eResult << std::endl;											\
	 		exit(1);}
#endif

#ifndef XMLCheckNullPtr
	#define XMLCheckNullPtr(a_ptr, a_eMessage) 										\
 		if (a_ptr == nullptr) { 																		\
 			std::cerr << "Xml error: " << a_eMessage << std::endl;		\
 			exit(1); }
#endif

using namespace std;

class XmlFile {

private:
	typedef vector<VictimPair> vecPair;
	typedef shared_ptr<VictimPair> vPairPtr;
	typedef shared_ptr<Host> hostPtr;

	string _filename;
	vecPair _victimPairs;
	ofstream _file2;
	xmlDocPtr _file;

	void _findOrNew(string name, hostPtr victim);
	hostPtr _loadVictim(xmlNodePtr nHost);

public:
	XmlFile(const string& filename);
	void openFile();
	void closeFile();
	void header();
	void devices();
	void devicesClose();
	void host(string macAddress);
	void hostClose();
	void ipv4(string ipv4Address);
	void ipv6(string ipv6Address);

	void loadFile();
	vecPair getVectorOfVictimPairs();
};

#endif