CXX := g++
CFLAGS := -g -Wall -std=c++11
INC := -I include -I/usr/include/libxml2/

SRCDIR := src
BUILDDIR := build
SRCEXT := cpp

TARGET_SCANNER := pds-scanner
TARGET_SPOOF := pds-spoof
TARGET_INTERCEPT := pds-intercept

SOURCES := $(shell find $(SRCDIR) -type f -name *.$(SRCEXT))
OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))

# Filter out spoof.o and intercept.o from scanner
SCANNER_OBJS := $(filter-out $(BUILDDIR)/pds-spoof.o, $(OBJECTS))
SCANNER_OBJS := $(filter-out $(BUILDDIR)/pds-intercept.o, $(SCANNER_OBJS))
# Filter out scanner.o and intercept.o from spoof
SPOOF_OBJS := $(filter-out $(BUILDDIR)/pds-scanner.o, $(OBJECTS))
SPOOF_OBJS := $(filter-out $(BUILDDIR)/pds-intercept.o, $(SPOOF_OBJS))
# Filter out scanner.o and spoof.o from intercept
INTERCEPT_OBJS := $(filter-out $(BUILDDIR)/pds-scanner.o, $(OBJECTS))
INTERCEPT_OBJS := $(filter-out $(BUILDDIR)/pds-spoof.o, $(INTERCEPT_OBJS))

all: $(TARGET_SCANNER) $(TARGET_SPOOF) $(TARGET_INTERCEPT)

$(TARGET_INTERCEPT): $(INTERCEPT_OBJS)
	@echo "\nLinking pds-intercept...";
	$(CXX) $^ -o $(TARGET_INTERCEPT) -lxml2

$(TARGET_SPOOF): $(SPOOF_OBJS)
	@echo "\nLinking pds-spoof...";
	$(CXX) $^ -o $(TARGET_SPOOF) -lxml2

$(TARGET_SCANNER): $(SCANNER_OBJS)
	@echo "\nLinking pds-scanner...";
	$(CXX) $^ -o $(TARGET_SCANNER) -lxml2

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	@mkdir -p $(BUILDDIR)
	$(CXX) $(CFLAGS) $(INC) -c -o $@ $< -lxml2

clean:
	@echo " Cleaning..."; 
	$(RM) -r $(BUILDDIR) $(TARGET_INTERCEPT) $(TARGET_SPOOF) $(TARGET_SCANNER)

.PHONY: clean

