# README #

Projekt do predmetu PDS - L2 MitM (Man in the Middle)
Autor: Matus Motlik, xmotli02@stud.fit.vutbr.cz
Datum: 09.04.2017


(1) Projekt obsahuje sadu programov pre uskutocnenie utoku MitM:
===============
	- program pre skenovanie lokalnej siete pomocou protokolov 
			ARP a ND.  
	- Program pre pravidelne otravovanie ARP a ND cache obeti
	- Program pre preposielanie paketov obeti


(2) Preklad:
===============
		$ make

	Vystupne binarky:
		- pds-scanner
		- pds-spoof
		- pds-intercept



(3) Spustenie:
===============
	Program musi byt spusteny s pravamy superuzivatele root:
	
	- $ ./pds-scanner -i interface -f filename

	- $ ./pds-spoof -i interface -t sec -p protocol 
				-victim1ip ipaddress -victim1mac macaddress 
				-victim2ip ipaddress -victim2mac macaddress 

	- $ ./pds-intercept -i interface -f filename


(4) Poznamky:
===============
	pds-scanner:
		- Program oskenuje lokalnu siet a vytvory xml subor obsahujuci 
			informacie o hostoch na sieti(MAC adresu a ipv4 a ipv6 adresy)
		- V pripade skenovania IPv6 siete je siet skenovana pomocou 
			multicastovej adresy ff02::1 – all nodes on link. Na tuto spravu 
			odpovie vacsina zariadeni na lokalnej sieti. Nie su vsak 
			detekovane zariadenia s OS Windows, globalne IPv6 adresy a ine.
	
	pds-spoof:
		- Program v zadanom intervale odosiela podvrhnute ARP/ND pakety obom 
			obetiam, cim im otravy ich cache.
		- Po ukonceni, program uvedie cache oboch obeti do povodneho stavu
	
	pds-intercept:
		- Program preposiela spravy medzi dvoma obetami, tak aby bola pre ne 
			zachovana iluzia, ze ich data nikto neodpocuva.