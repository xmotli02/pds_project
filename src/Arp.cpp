/**
 * Class for arp packet
 *
 * Author: Matus Motlik, xmotli02@stud.fit.vutbr.cz
 * 
 */

#include "Arp.h"

const uint16_t Arp::REQUEST = 1;
const uint16_t Arp::REPLY = 2;
const int Arp::ARP_HDR_L = 28;


Arp::Arp() {
	this->_type = ETH_P_ARP;
}

/**
 * Save arp header into private struct
 */
Arp::Arp(uint32_t destIp, uint8_t* destMac, 
				 uint32_t sourceIp, uint8_t* sourceMac, 
				 uint16_t operation) : Arp() {

	// Hardware type (16 bits): 1 for ethernet
	_arpHeader.htype = htons(1);
	// Protocol type (16 bits): 2048 for IP
	_arpHeader.ptype = htons(ETH_P_IP);
	// Hardware address length (8 bits): 6 bytes for MAC address
	_arpHeader.hlen = 6;
	// Protocol address length (8 bits): 4 bytes for IPv4 address
	_arpHeader.plen = 4;
	// OpCode: 1 for ARP request
	_arpHeader.opCode = htons(operation);
	// Source
	// _arpHeader.sourceIp = sourceIp;
	memcpy(&_arpHeader.sourceIp, &sourceIp, sizeof(uint32_t));
  memcpy(&_arpHeader.sourceMac, sourceMac, 6 * sizeof(uint8_t));
  // Destination
  // _arpHeader.destIp = destIp;
	memcpy(&_arpHeader.destIp, &destIp, sizeof(uint32_t));
  memcpy(&_arpHeader.destMac, destMac, 6 * sizeof(uint8_t));
}

int Arp::getBufferSize() {
	return this->_bufferSize;
}

void Arp::setBufferSize(int size) {
	this->_bufferSize = size;
}

uint16_t Arp::getType() {
	return this->_type;
}

string Arp::getSourceIp() {
	ostringstream convert;
	int i;

	for (i = 0; i < 3; i++) {
		convert << (int)this->_arpHeader.sourceIp[i] << ".";
	}
	convert << (int)this->_arpHeader.sourceIp[i];

	return convert.str();
	// return inet_ntoa(*(struct in_addr *)&(this->_arpHeader.sourceIp));
}

string Arp::getDestinationIp() {
	ostringstream convert;
	int i;

	for (i = 0; i < 3; i++) {
		convert << (int)this->_arpHeader.destIp[i] << ".";
	}
	convert << (int)this->_arpHeader.destIp[i];

	return convert.str();
}

string Arp::getSourceMac() {
	ostringstream convert;
	int i;

	for (i = 0; i < 6; i++) {
		convert << setfill('0') << setw(2) << hex << (int)this->_arpHeader.sourceMac[i];
		i++;
		convert << setfill('0') << setw(2) << hex << (int)this->_arpHeader.sourceMac[i];
		if (i < 4) {
			convert << ".";
		}
	}

	return convert.str();
}

/**
 * Create ethernet frame with arp packet
 * 
 * source: https://gist.github.com/seungwon0/7110259
 * @return Etherned frame
 */
uint8_t* Arp::getBuffer() {
	uint8_t* etherFrame = new uint8_t[IP_MAXPACKET];

	// Ethernet frame length = ethernet header (MAC + MAC + ethernet type) + ethernet data (ARP header)
	this->setBufferSize(ETHER_HDR_L + ARP_HDR_L);
	// Destination and Source MAC addresses
  memcpy(etherFrame, this->_arpHeader.destMac, 6 * sizeof(uint8_t));
  memcpy(etherFrame + 6, this->_arpHeader.sourceMac, 6 * sizeof(uint8_t));

	// Ethernet type code (ETH_P_ARP for ARP).
  // http://www.iana.org/assignments/ethernet-numbers
  etherFrame[12] = ETH_P_ARP / 256;
  etherFrame[13] = ETH_P_ARP % 256;

  // Copy arp header to etherFrame
  memcpy(etherFrame + ETHER_HDR_L, 
  	&_arpHeader, ARP_HDR_L * sizeof(uint8_t));

	return etherFrame;
}

/**
 * Save data from arp header
 * @param buffer ethernet frame
 */
void Arp::setData(uint8_t* buffer) {
	// Remove ethernet header
	uint8_t* buf = buffer + Packet::ETHER_HDR_L;

	ArpHeader *tmp; 
	tmp = (struct ArpHeader*)buf;

	_arpHeader.htype = ntohs(tmp->htype);
	_arpHeader.ptype = ntohs(tmp->ptype);
	_arpHeader.hlen = tmp->hlen;
	_arpHeader.plen = tmp->plen;
	_arpHeader.opCode = htons(tmp->opCode);
	memcpy(&_arpHeader.sourceIp, &tmp->sourceIp, sizeof(uint32_t));
  memcpy(&_arpHeader.sourceMac, tmp->sourceMac, 6 * sizeof(uint8_t));
	memcpy(&_arpHeader.destIp, &tmp->destIp, sizeof(uint32_t));
  memcpy(&_arpHeader.destMac, tmp->destMac, 6 * sizeof(uint8_t));
}
