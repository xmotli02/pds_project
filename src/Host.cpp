/**
 * Host class reprezenting single host in local network
 *
 * Author: Matus Motlik, xmotli02@stud.fit.vutbr.cz
 * Description: 
 * 
 */

#include "Host.h"

Host::Host() {}

Host::Host(const string& macAddress): Host() {
	_macAddress = macAddress;
}

bool Host::operator == (const Host &host) {
	return _macAddress == host._macAddress;
}

string Host::getMacAddress() {
	return _macAddress;
}

/**
 * Convert mac address in string format to vector<uint8_t>
 */
vector<uint8_t> Host::getMacAsVector() {
	int tmp[6];
	vector<uint8_t> macVector;

	sscanf(this->getMacAddress().c_str(), 
				 "%02x%02x.%02x%02x.%02x%02x", 
				 &tmp[0], &tmp[1], &tmp[2], &tmp[3], &tmp[4], &tmp[5]);
	macVector.assign(&tmp[0], &tmp[6]);

	return macVector;
}

void Host::setMacAddress(const string& mac) {
	_macAddress = mac;
}

/**
 * Return IPv4/IPv6 address of host
 * @return IP address string
 */
string Host::getIpAddress() {
	return _ipAddress;
}

void Host::setIpAddress(const string& ip) {
	_ipAddress = ip;
}

vector<string> Host::getIpv4Addresses() {
	return _ipv4;
}

void Host::addIpv4(const string& ipv4) {
	if (find(_ipv4.begin(), _ipv4.end(), ipv4) == _ipv4.end()) {
		_ipv4.push_back(ipv4);
	}
}

vector<string> Host::getIpv6Addresses() {
	return _ipv6;
}

void Host::addIpv6(const string& ipv6) {
	if (find(_ipv6.begin(), _ipv6.end(), ipv6) == _ipv6.end()) {
		_ipv6.push_back(ipv6);
	}
}

/**
 * Compare all ipv4 and ipv6 address to given ip address
 * @param  ip IPv4/IPv6 address
 * @return    true/false
 */
bool Host::isAddressOfHost(string ip) {
	// IPv4
	for (auto addr : this->getIpv4Addresses()) {
		if (addr.compare(ip) == 0) {
			return true;
		}
	}
	// IPv6
	for (auto addr : this->getIpv6Addresses()) {
		if (addr.compare(ip) == 0) {
			return true;
		}
	}

	return false;
}
