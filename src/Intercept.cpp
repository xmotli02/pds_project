/**
 * Spoof singleton class used for spoofing 
 *
 * Author: Matus Motlik, xmotli02@stud.fit.vutbr.cz
 */

#include "Intercept.h"

string Intercept::getFileName() { 
	return this->_fileName; 
} 

void Intercept::setFileName(const string& fileName) { 
	this->_fileName = fileName; 
} 

shared_ptr<Interface> Intercept::getInterface() {
	return this->_interface;
}

void Intercept::setInterface(const string& interfaceName) {
	shared_ptr<Interface> interface(new Interface(interfaceName));
	this->_interface = move(interface);
}


vector<VictimPair> Intercept::getVictimPairs() {
	return this->_victimPairs;
}

void Intercept::setVictimPairs(vector<VictimPair> victimPairs) {
	this->_victimPairs = victimPairs;
}

/**
 * Intercept on given interface 
 */
void Intercept::interceptOnInterface() {
	XmlFile xmlFile(this->getFileName());
	xmlFile.loadFile();
	// Get vector of victim pairs from xml file
	this->_victimPairs = xmlFile.getVectorOfVictimPairs();
	// Create socket
	getInterface()->createSocket();
	// Listen on socket
	while(1) {
		// sniff on interface and set callback function
		getInterface()->sniffOnInterface(
				bind(&Intercept::gotPacket, this, placeholders::_1, placeholders::_2));
	}
}

/**
 * Repair packet destination mac address if belongs to victim
 * @param buffer recieved packet
 */
void Intercept::gotPacket(uint8_t* buffer, int bufferSize) {
	// Get type of packet
	uint16_t type;
	memcpy(&type, buffer+12, sizeof(uint16_t));
	type = ntohs(type);

	switch(type) {
		// ipv4
		case ETH_P_IP: {
			Ipv4 ipv4;
			ipv4.setData(buffer);
			if (this->isPacketFromVictimPair(buffer, &ipv4)) {
				// Send packet to second victim 
				this->getInterface()->sendBuffer(buffer, bufferSize);
			}
			break;
		}
		// ipv6
		case ETH_P_IPV6: {
			Ipv6 ipv6;
			ipv6.setData(buffer);
			if (this->isPacketFromVictimPair(buffer, &ipv6)) {
				// Send packet to second victim 
				this->getInterface()->sendBuffer(buffer, bufferSize);
			}
			break;
		}
		default:
			break;
	}
}

/**
 * Check if is packet from victim pair, then repair destination 
 * 	MAC address in received buffer 
 * @param  buffer received data
 * @param  packet ipv4/ipv6 packet
 * @return        
 */
bool Intercept::isPacketFromVictimPair(uint8_t* buffer, Packet *packet) {
	// Get src MAC and dst ip address
	string srcMac = packet->getSourceMac();
	string dstIp = packet->getDestinationIp();
	uint8_t dstMac[6] = {0,0,0,0,0,0};

	// Check if src and dst are victims
	for (auto &pair : this->_victimPairs) {  
		if (pair.isInVictimPair(srcMac, dstIp, dstMac)) {
			// Fix wrong MAC address
			cout << "Replacing wrong mac for victim:" << dstIp << endl;
			memcpy(buffer, dstMac, 6);

			return true;
		}
	}
	
	return false;
}