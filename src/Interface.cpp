/**
 * Interface class reprezenting interface and used for communication
 *
 * Author: Matus Motlik, xmotli02@stud.fit.vutbr.cz
 * 
 */

#include "Interface.h"

Interface::Interface(const string& name) {
	this->_interfaceName = name;

	this->_getIpAddresses();
	this->_getMacAddress();
}

/**
 * Get Ipv4 and Ipv6 addresses
 * Source: http://stackoverflow.com/a/265978/4156870
 */
void Interface::_getIpAddresses() {
	struct ifaddrs* ifAddrStruct = NULL;
	struct ifaddrs* ifa = NULL;

	getifaddrs(&ifAddrStruct);

	for (ifa = ifAddrStruct; ifa != NULL; ifa = ifa->ifa_next) {
		if (!ifa->ifa_addr || ifa->ifa_name != this->getName()) {
			continue;
		}

		// IP4 address
		if (ifa->ifa_addr->sa_family == AF_INET) { 
			struct in_addr* tmpAddrPtr = NULL;
			tmpAddrPtr = &((struct sockaddr_in *) ifa->ifa_addr)->sin_addr;
			this->_ipv4Address = (uint32_t)tmpAddrPtr->s_addr;

			// Network mask
			tmpAddrPtr = &((struct sockaddr_in *) ifa->ifa_netmask)->sin_addr;
			this->_ipv4Mask = (uint32_t)tmpAddrPtr->s_addr;
		} 
		// Ipv6 address
		else if (ifa->ifa_addr->sa_family == AF_INET6) { 
			struct in6_addr* tmpAddrPtr = NULL;
			tmpAddrPtr = &((struct sockaddr_in6 *) ifa->ifa_addr)->sin6_addr;
			memcpy(this->_ipv6Address, tmpAddrPtr->s6_addr, 16 * sizeof(uint8_t));

			// Network mask
			tmpAddrPtr = &((struct sockaddr_in6 *) ifa->ifa_netmask)->sin6_addr;
			char addressBuffer[INET6_ADDRSTRLEN];
			inet_ntop(AF_INET6, tmpAddrPtr, addressBuffer, INET6_ADDRSTRLEN);
			this->_ipv6Mask = addressBuffer;
		} 
	}

	if (ifAddrStruct!=NULL) {
		freeifaddrs(ifAddrStruct);
	}
}

/**
 * Save MAC address of interface
 */
void Interface::_getMacAddress() {
	struct ifreq ifr;
	int sd;

	// Socket descriptor to look up interface.
	if ((sd = socket(AF_INET, SOCK_RAW, IPPROTO_RAW)) < 0) {
		cerr << "socket() failed to get descriptor for using ioctl()" << endl;
		exit(1);
	}

	// Mac address
	memset(&ifr, 0, sizeof(ifr));
	snprintf(ifr.ifr_name, sizeof(ifr.ifr_name), "%s", this->getName().c_str());
	if (ioctl(sd, SIOCGIFHWADDR, &ifr) < 0) {
		cerr << "ioctl() failed to get source MAC address" << endl;
		exit(1);
	}

	close(sd);
	// Copy MAC address.
	memcpy(this->_macAddress, ifr.ifr_hwaddr.sa_data, 6 * sizeof(uint8_t));
}

/**
 * Get name of interface
 * @return interface name
 */
string Interface::getName() {
	return this->_interfaceName;
}

uint32_t Interface::getIpv4Address() {
	return (this->_ipv4Address);
}

uint8_t* Interface::getMacAddress() {
	return (this->_macAddress);
}

uint32_t Interface::getIpv4Mask() {
	return this->_ipv4Mask;
}

uint8_t* Interface::getIpv6Address() {
	return this->_ipv6Address;
}

string Interface::getIpv6Mask() {
	return this->_ipv6Mask;
}


/**
 * Get total number of possible hosts in network
 * @return bitwise inversion of network mask - 1
 */
int Interface::getTotalIpv4Hosts() {
	return ~ntohl(this->getIpv4Mask()) - 1;
}

/**
 * Get first possible host ip address in local network
 * @return First host ip address
 */
uint32_t Interface::getFirstHostIpv4() {
	uint32_t networkAddress = this->getIpv4Mask() & this->getIpv4Address();
	networkAddress = ntohl(networkAddress) + 1;
	return htonl(networkAddress);
}

/**
 * Convert IPv4 string to uint32_t
 * @param  ipv4 
 * @return converted ipv4 address
 */
uint32_t Interface::convertStringIpv4(string ipv4) {
	uint32_t address;
	if (inet_pton(AF_INET, ipv4.c_str(), &address) != 1) {
		cerr << "Wrong IPv4 address: " << ipv4 << endl;
		exit(1);
	}
	return address;
}

/**
 * Convert IPv6 string to uint8_t array
 * @param  ipv6
 * @return converted ipv6 address
 */
vector<uint8_t> Interface::convertStringIpv6(string ipv6) {
	uint8_t tmpAddrPtr[16];
	if (inet_pton(AF_INET6, ipv6.c_str(), tmpAddrPtr) != 1) {
		cerr << "Wrong IPv6 address: " << ipv6 << endl;
		exit(1);
	}

	vector<uint8_t> address(tmpAddrPtr, tmpAddrPtr + 16);
	return address;
}

/**
 * Create socket
 */
void Interface::createSocket() {
	cout << "Creating socket" << endl;
	this->_socket = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
  if (this->_socket < 0) {
    cerr << "socket() failed" << endl;
    exit(1);
  }
}

int Interface::getSocket() {
	return this->_socket;
}

void Interface::closeSocket() {
	cout << "Closing socket" << endl;
	close(this->_socket);
}

/**
 * Send created buffer
 * @param buffer     
 * @param bufferSize 
 */
void Interface::sendBuffer(uint8_t* buffer, int bufferSize) {
  struct sockaddr_ll device;

	// Find interface index from interface name and store index in
  // struct sockaddr_ll device, which will be used as an argument of sendto().
  if ((device.sll_ifindex = if_nametoindex(this->getName().c_str())) == 0) {
    cerr << "if_nametoindex() failed to obtain interface index" << endl;
    exit(1);
  }

  // Fill out sockaddr_ll.
  device.sll_family = AF_PACKET;
  memcpy(device.sll_addr, this->getMacAddress(), 6 * sizeof(uint8_t));
  device.sll_halen = htons(6);

	short bytes = sendto(this->getSocket(), buffer, bufferSize, 0, 
			(struct sockaddr *) &device, sizeof(device));
	
	if (bytes <= 0) {
		cerr << "sendto() failed" << endl;
		exit(1);
	}

  // delete buffer;
}

/**
 * Send packet to created socket
 * @param packet arp/ipv6 packet
 */
void Interface::sendPacket(Packet* packet) {
	// Get buffer filled with data
	uint8_t *buffer = packet->getBuffer();
	// Get buffer size
	int bufferSize = packet->getBufferSize();
	// Send created buffer
	this->sendBuffer(buffer, bufferSize);
}


bool Interface::waitForReply(Packet* packet) {
	struct sockaddr_storage sender;
	socklen_t sendSize = sizeof(sender);
	bzero(&sender, sizeof(sender));
	uint8_t buffer[IP_MAXPACKET];

	struct timeval tv;
	tv.tv_sec = 0;
	tv.tv_usec = 10000;

	if (setsockopt(this->getSocket(), SOL_SOCKET, SO_RCVTIMEO,&tv,sizeof(tv)) < 0) {
		cerr << "Error setsockopt()" << endl;
	}

	int messageLength = recvfrom(this->getSocket(), buffer, sizeof(buffer), 0, (struct sockaddr*)&sender, &sendSize);

	if (messageLength > 0) {
		// Read ethernet type field
		uint16_t type;
		memcpy(&type, buffer+12, sizeof(uint16_t));
		type = ntohs(type);

		// Only arp and ndp
		if (type == packet->getType()) {
			packet->setData(buffer);
			return true;
		}
	}
	return false;
}

/**
 * Sniff on interface and call callback function when receive packet
 * @param callback [description]
 */
void Interface::sniffOnInterface(function<void(uint8_t*, int)> callback) {
	struct sockaddr_storage sender;
	socklen_t sendSize = sizeof(sender);
	bzero(&sender, sizeof(sender));
	uint8_t buffer[IP_MAXPACKET];

	struct timeval tv;
	tv.tv_sec = 0;
	tv.tv_usec = 10000;

	if (setsockopt(this->getSocket(), SOL_SOCKET, SO_RCVTIMEO,&tv,sizeof(tv)) < 0) {
		cerr << "Error setsockopt()" << endl;
	}

	int messageLength = recvfrom(this->getSocket(), buffer, sizeof(buffer), 0, (struct sockaddr*)&sender, &sendSize);

	if (messageLength > 0) {
		callback(buffer, messageLength);
	}
}