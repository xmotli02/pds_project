/**
 * Class for Ipv4 packet
 *
 * Author: Matus Motlik, xmotli02@stud.fit.vutbr.cz
 * 
 */

#include "Ipv4.h"

Ipv4::Ipv4() {
	// Ethernet type code (ETH_P_IP for Ipv4).
  // http://www.iana.org/assignments/ethernet-numbers
	this->_type = ETH_P_IP;
}

int Ipv4::getBufferSize() {
	return this->_bufferSize;
}

void Ipv4::setBufferSize(int size) {
	this->_bufferSize = size;
}

uint16_t Ipv4::getType() {
	return this->_type;
}

string Ipv4::getSourceIp() {
	return inet_ntoa(this->_ipv4Header.ip_src);
}

string Ipv4::getDestinationIp() {
	return inet_ntoa(this->_ipv4Header.ip_dst);
}

string Ipv4::getSourceMac() {
	ostringstream convert;
	int i;

	for (i = 0; i < 6; i++) {
		convert << setfill('0') << setw(2) << hex << (int)this->srcMac[i];
		i++;
		convert << setfill('0') << setw(2) << hex << (int)this->srcMac[i];
		if (i < 4) {
			convert << ".";
		}
	}
	return convert.str();
}

uint8_t* Ipv4::getBuffer() {
	return nullptr;
}

/**
 * Save data from ethernet frame header and Ipv4 header
 * @param buffer ethernet frame
 */
void Ipv4::setData(uint8_t* buffer) {
	// Save source MAC address from ethernet header
	memcpy(this->srcMac, buffer + 6, 6 * sizeof(uint8_t));
	// Remove ethernet header
	uint8_t* buf = buffer + Packet::ETHER_HDR_L;
	// Save Ipv4 header
	memcpy(&this->_ipv4Header, (struct ip*)buf, sizeof(ip));
}
