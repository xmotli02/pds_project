/**
 * Class for Ipv6 packet
 *
 * Author: Matus Motlik, xmotli02@stud.fit.vutbr.cz
 * 
 */

#include "Ipv6.h"

const uint8_t Ipv6::REQUEST = ICMP6_ECHO_REQUEST;
const uint8_t Ipv6::REPLY = ICMP6_ECHO_REPLY;
const uint8_t Ipv6::ADVERTISEMENT = ND_NEIGHBOR_ADVERT;

const int Ipv6::IPV6_HDR_L = 40;
const int Ipv6::ICMP6_HDR_L = 8;
const int Ipv6::ICMP6_TARGET_L = 16; // Target = ipv6 address
const int Ipv6::ICMP6_OPTIONS_L = 8; // Options = Options + Link layer address


Ipv6::Ipv6() {
	// Ethernet type code (ETH_P_IPV6 for IPv6).
  // http://www.iana.org/assignments/ethernet-numbers
	this->_type = ETH_P_IPV6;
}

Ipv6::Ipv6(uint8_t* dstIp, uint8_t* dstMac, uint8_t* srcIp, uint8_t* srcMac, uint8_t operation): Ipv6() {

	this->_setIcmp6Type(operation);
	this->_setIPv6Header(srcIp, dstIp);
	this->_setIcmp6Header(srcIp, srcMac, operation);

  memcpy(this->srcMac, srcMac, 6 * sizeof(uint8_t));
  memcpy(this->dstMac, dstMac, 6 * sizeof(uint8_t));
}

void Ipv6::_setIcmp6Type(uint8_t type) {
	this->_icmp6Header.icmp6_type = type;
}

uint8_t Ipv6::_getIcmpType() {
	return this->_icmp6Header.icmp6_type;
}

/**
 * Get icmp6 header length based on type of icmp
 * @return length of icmp6 header
 */
uint16_t Ipv6::_getIcmpHeaderLenght() {
	if (this->_getIcmpType() == ADVERTISEMENT) {
		return ICMP6_HDR_L + ICMP6_TARGET_L + ICMP6_OPTIONS_L;
	}

	return ICMP6_HDR_L;
}

int Ipv6::getBufferSize() {
	return this->_bufferSize;
}

void Ipv6::setBufferSize(int size) {
	this->_bufferSize = size;
}

void Ipv6::_extendBufferSize(int size) {
	this->_bufferSize += size;
}

void Ipv6::_copyToBuffer(uint8_t* buffer, void* data, int dataLength) {
	memcpy(buffer + this->getBufferSize(), data, dataLength);
	this->_extendBufferSize(dataLength);
}

uint16_t Ipv6::getType() {
	return this->_type;
}

string Ipv6::getSourceIp() {
	char ipv6Str[INET6_ADDRSTRLEN];
	inet_ntop(AF_INET6, &this->_ipv6Header.ip6_src, ipv6Str, INET6_ADDRSTRLEN);
	return ipv6Str;
}

string Ipv6::getDestinationIp() {
	char ipv6Str[INET6_ADDRSTRLEN];
	inet_ntop(AF_INET6, &this->_ipv6Header.ip6_dst, ipv6Str, INET6_ADDRSTRLEN);
	return ipv6Str;
}

string Ipv6::getSourceMac() {
	ostringstream convert;
	int i;

	for (i = 0; i < 6; i++) {
		convert << setfill('0') << setw(2) << hex << (int)this->srcMac[i];
		i++;
		convert << setfill('0') << setw(2) << hex << (int)this->srcMac[i];
		if (i < 4) {
			convert << ".";
		}
	}
	return convert.str();
}

void Ipv6::_setIPv6Header(uint8_t* srcIpv6, uint8_t* dstIpv6) {
	// IPv6 version (4 bits), Traffic class (8 bits), Flow label (20 bits)
	this->_ipv6Header.ip6_flow = htonl((6 << 28) | (0 << 20) | 0);
	// Payload length (16 bits): ICMP header
	this->_ipv6Header.ip6_plen = htons(this->_getIcmpHeaderLenght());
	// Next header (8 bits): 58 for ICMP
	this->_ipv6Header.ip6_nxt = IPPROTO_ICMPV6;
	// Hop limit (8 bits): default to maximum value
	this->_ipv6Header.ip6_hops = 255;
	// Source IPv6 address (128 bits)
	memcpy(&(this->_ipv6Header.ip6_src), srcIpv6, 16 * sizeof(uint8_t));
	// Destination IPv6 address (128 bits)
	memcpy(&(this->_ipv6Header.ip6_dst), dstIpv6, 16 * sizeof(uint8_t));
}

/**
 * Set icmp6 header for reply/advertisement
 * Source: https://tools.ietf.org/html/rfc4861#page-23
 * @param dstIp  
 * @param dstMac 
 * @param type 
 */
void Ipv6::_setIcmp6Header(uint8_t* dstIp, uint8_t* dstMac, uint8_t type) {
	// Message Type (8 bits): echo request
	this->_icmp6Header.icmp6_type = type;
	// Message Code (8 bits): echo request
	this->_icmp6Header.icmp6_code = 0;
	// ICMP header checksum (16 bits): set to 0 when calculating checksum
	this->_icmp6Header.icmp6_cksum = 0;

	// ToDo: dataUnspecified - when ADVERTISEMENT there is sth else (3 bits flags)
	// // Identifier (16 bits): usually pid of sending process - pick a number
	this->_icmp6Header.icmp6_id = htons(0);
	// // Sequence Number (16 bits): starts at 0
	this->_icmp6Header.icmp6_seq = htons(0);

	if (this->_getIcmpType() == ADVERTISEMENT) {
		this->_icmp6Header.icmp6_id |= (3u << 5); 	// set Sol and Ovr flag
		this->_setIcmp6Target(dstIp);
		this->_setIcmp6Options(dstMac);
	}

	// Compute checksum
	this->_icmp6Header.icmp6_cksum = _icmp6Checksum();
}

/**
 * Set target field in icmp6 for neighbor ADVERTISEMENT
 * @param target target address
 */
void Ipv6::_setIcmp6Target(uint8_t* target) {
	memcpy(&(this->_icmp6Target.s6_addr), target, 16 * sizeof(uint8_t));
}

/**
 * Set options field in icmp6 for neighbor ADVERTISEMENT
 */
void Ipv6::_setIcmp6Options(uint8_t* mac) {
	this->_icmp6Options.nd_opt_type = 2;
	this->_icmp6Options.nd_opt_len = 1;
	// Source Link local address
	memcpy(this->_icmp6MacAddr, mac, 6 * sizeof(uint8_t));
}

/**
 * Get checksum of icmp6 and pseudo ipv6 header
 * @return Computed checksum
 */
uint16_t Ipv6::_icmp6Checksum() {
	uint8_t buffer[IP_MAXPACKET];
	uint8_t* ptr = &buffer[0];
	int lenght = 0;

	// Copy IPv6 pseudo header to buffer
	// Copy source IP address into buffer (128 bits)
	memcpy(ptr, &this->_ipv6Header.ip6_src.s6_addr, 16 * sizeof(uint8_t));
	ptr += 16 * sizeof(uint8_t);
	// Copy destination IP address into buffer (128 bits)
	memcpy(ptr, &this->_ipv6Header.ip6_dst.s6_addr, 16 * sizeof(uint8_t));
	ptr += 16 * sizeof(uint8_t);
	// Copy Upper Layer Packet length into buffer (32 bits).
	*ptr = 0; ptr++;
	*ptr = 0; ptr++;
	*ptr = this->_getIcmpHeaderLenght() / 256; ptr++;
	*ptr = this->_getIcmpHeaderLenght() % 256; ptr++;
	// Copy zero field to buffer (24 bits)
	*ptr = 0; ptr++;
	*ptr = 0; ptr++;
	*ptr = 0; ptr++;
	// Copy next header field to buffer (8 bits)
	memcpy(ptr, &this->_ipv6Header.ip6_nxt, sizeof(uint8_t));
	ptr++;
	
	// Copy ICMPv6 header to buffer
	memcpy(ptr, &this->_icmp6Header, sizeof(icmp6_hdr));
	ptr += sizeof(icmp6_hdr);
  
	// Advertisement icmp6 
  if (this->_getIcmpType() == ADVERTISEMENT) {
  	// Target field
  	memcpy(ptr, &this->_icmp6Target, sizeof(in6_addr));
		ptr += sizeof(in6_addr);
		// Options field
		memcpy(ptr, &this->_icmp6Options, sizeof(nd_opt_hdr));
		ptr += sizeof(nd_opt_hdr);
		// Options link layer address
		memcpy(ptr, this->_icmp6MacAddr, 6 * sizeof(uint8_t));
		ptr += 6 * sizeof(uint8_t);
  }
  else {
	  // Copy ICMPv6 data to buffer
		memset(ptr, 0, sizeof(uint32_t));
	}
	// Compute length of buffer
	lenght = (long)ptr - (long)buffer;

	return _checksum((uint16_t*)buffer, lenght);
}

/**
 * Compute checksum of buffer
 * Source: https://www.cs.utah.edu/~swalton/listings/sockets/programs/part4/chap18/myping.c
 */
uint16_t Ipv6::_checksum(uint16_t* buffer, int lenght) {
	uint32_t sum = 0;
	uint16_t result;

	for ( sum = 0; lenght > 1; lenght -= 2 ) {
		sum += *buffer++;
	}

	if ( lenght == 1 ) {
		sum += *(uint8_t*)buffer;
	}

	sum = (sum >> 16) + (sum & 0xFFFF);
	sum += (sum >> 16);
	result = ~sum;
	return result;
}


/**
 * Create Ethernet frame header
 * @param etherFrame buffer
 * @param srcMac     
 * @param dstMac     
 * @param type 				     
 */
void Ipv6::_copyEtherFrameToBuffer(uint8_t* etherFrame, 
						uint8_t* srcMac, uint8_t* dstMac, uint16_t type) {
	// Destination and Source MAC addresses
	memcpy(etherFrame, dstMac, 6 * sizeof(uint8_t));
	memcpy(etherFrame + 6, srcMac, 6 * sizeof(uint8_t));

	etherFrame[12] = type / 256;
	etherFrame[13] = type % 256;
}


/**
 * Create Ethernet frame with IPv6 and ICMPv6
 * @return Created ethernet frame
 */
uint8_t* Ipv6::getBuffer() {
	uint8_t* buffer = new uint8_t[IP_MAXPACKET];
	//Create ethernet frame
	_copyEtherFrameToBuffer(buffer, this->srcMac, this->dstMac, this->getType());
	// Set buffer size to ether header length
	this->setBufferSize(ETHER_HDR_L);
	// Copy ipv6 header to buffer
	this->_copyToBuffer(buffer, &_ipv6Header, IPV6_HDR_L);
	// Copy icmp header to buffer
	this->_copyToBuffer(buffer, &_icmp6Header, ICMP6_HDR_L);

	// If icmp type == ADVERTISEMENT
	if (this->_getIcmpType() == ADVERTISEMENT) {
		// Copy icmp target to buffer
		this->_copyToBuffer(buffer, &_icmp6Target, sizeof(in6_addr));
		// Copy icmp options to buffer
		this->_copyToBuffer(buffer, &_icmp6Options, sizeof(nd_opt_hdr));
		// Copy icmp target mac to buffer
		this->_copyToBuffer(buffer, _icmp6MacAddr, 6 * sizeof(uint8_t));
	}
	else {
		// Set icmp6 data field in buffer to 0
		memset(buffer + this->getBufferSize(), 0, sizeof(uint32_t));
		// this->_extendBufferSize(sizeof(uint32_t));
	}

	return buffer;
}

/**
 * Save data from ethernet frame header and IPv6 header
 * @param buffer ethernet frame
 */
void Ipv6::setData(uint8_t* buffer) {
	// Save source MAC address from ethernet header
	memcpy(this->srcMac, buffer + 6, 6 * sizeof(uint8_t));
	// Remove ethernet header
	uint8_t* buf = buffer + Packet::ETHER_HDR_L;
	// Save IPv6 header
	memcpy(&this->_ipv6Header, (struct ip6_hdr*)buf, sizeof(ip6_hdr));
}
