/**
 * Scanner singleton class used for scanning local network
 *
 * Author: Matus Motlik, xmotli02@stud.fit.vutbr.cz
 */

#include "Scanner.h"

string Scanner::getFileName() { 
	return this->_fileName; 
} 

void Scanner::setFileName(const string& fileName) { 
	this->_fileName = fileName; 
} 

Interface* Scanner::getInterface() {
	return this->_interface;
}

void Scanner::setInterface(const string& interfaceName) { 
	this->_interface = new Interface(interfaceName);
} 

/**
 * Get all found hosts in local network
 */
vector<Host> Scanner::getHosts() { 
	return this->_hosts; 
} 

/**
 * ToDo: mozno najst lepsie riesenie, nie cez vector (set/map) !!!!!!!!!!!!!!!!
 * Find or create new Host object in vector
 * 
 * @param  macAddress Mac address of Host
 * @return pointer to found/new Host object
 */
Host& Scanner::findOrNewHost(string macAddress) { 
  vector<Host>::iterator it;
  Host newHost(macAddress);
  it = find(_hosts.begin(), _hosts.end(), newHost);

  if (it != _hosts.end()) {
    return *it;
  }
  else {
  	_hosts.insert(it, newHost);
  }

  return _hosts.back();
} 

/**
 * Scan local network and create list of found hosts
 */
void Scanner::scanNetwork() {
	// Set destination MAC address: broadcast address
	uint8_t broadcastMac[6];
  memset(broadcastMac, 0xff, 6 * sizeof(uint8_t));

	// Scan network for all possible hosts in IPv4
	this->getInterface()->createSocket();

	int totalNumberOfHosts = this->getInterface()->getTotalIpv4Hosts();
	uint32_t destIp = this->getInterface()->getFirstHostIpv4();

	cout << "Scanning IPv4 local network." << endl;

	for (int i = 1; i <= totalNumberOfHosts; i++) {
		// Create arp request packet
		Arp arpRequest(destIp,
						broadcastMac,
						this->getInterface()->getIpv4Address(), 
						this->getInterface()->getMacAddress(),
						Arp::REQUEST);
		// Send ARP request
		this->getInterface()->sendPacket(&arpRequest);

		Arp arpReply;
		if (this->getInterface()->waitForReply(&arpReply)) {
			// save to found hosts
	  	Host &newHostIpv4 = this->findOrNewHost(arpReply.getSourceMac());
			newHostIpv4.addIpv4(arpReply.getSourceIp());
		}

		destIp = ntohl(destIp);
		destIp++;
		destIp = htonl(destIp);
	} 

	cout << "IPv4 network scanning finnished." << endl;
	cout << "Scanning IPv6 local network." << endl;
	
	// Scan network for all possible hosts in IPv6
	// ToDo:
	uint8_t destIpv6[16] = {0xff, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01};

	// Create Ipv6 packet
	Ipv6 ipv6Request(destIpv6,
					 broadcastMac,
					 this->getInterface()->getIpv6Address(), 
					 this->getInterface()->getMacAddress(),
					 Ipv6::REQUEST);

	// Send packet
	this->getInterface()->sendPacket(&ipv6Request);
	// Read reply
	Ipv6 ipv6Reply;
	while (this->getInterface()->waitForReply(&ipv6Reply)) {
		// Save found hosts
		Host &newHostIpv6 = this->findOrNewHost(ipv6Reply.getSourceMac());
		newHostIpv6.addIpv6(ipv6Reply.getSourceIp());
	}
	
	cout << "IPv6 network scanning finnished." << endl;
	this->getInterface()->closeSocket();
}

/**
 * Create new xml file with info about hosts in local network
 */
void Scanner::createXmlFile() {
	cout << "Creating xml file!" << endl;

	XmlFile xmlFile(this->getFileName());

	xmlFile.openFile();
	xmlFile.header();
	xmlFile.devices();

	for (auto &host : this->getHosts()) {
		xmlFile.host(host.getMacAddress());
		
		for (string &ipv4: host.getIpv4Addresses()) {
			xmlFile.ipv4(ipv4);
		}
		
		for (string &ipv6: host.getIpv6Addresses()) {
			xmlFile.ipv6(ipv6);
		}
		
		xmlFile.hostClose();
	}

	xmlFile.devicesClose();
	xmlFile.closeFile();
}
