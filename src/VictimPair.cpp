/**
 * Class representing victim pair
 *
 * Author: Matus Motlik, xmotli02@stud.fit.vutbr.cz
 */

#include "VictimPair.h"

VictimPair::VictimPair() { }

VictimPair::VictimPair(const string& name): VictimPair() {
	_name = name;
}

bool VictimPair::operator == (const VictimPair &pair) {
	return _name == pair._name;
}

void VictimPair::setName(const string& name) {
	this->_name = name;
}

string VictimPair::getName() {
	return this->_name;
}

shared_ptr<Host> VictimPair::getVictim1() {
	return this->_firstVictim;
}

void VictimPair::setVictim1(shared_ptr<Host> victim) {
	this->_firstVictim = victim;
}

shared_ptr<Host> VictimPair::getVictim2() {
	return this->_secondVictim;
}

void VictimPair::setVictim2(shared_ptr<Host> victim) {
	this->_secondVictim = victim;
}

void VictimPair::insertVictim(shared_ptr<Host> victim) {
	if (this->getVictim1() == nullptr) {
		this->setVictim1(victim);
	}
	else {
		this->setVictim2(victim);
	}
}

/**
 * Search if IP addresses belongs to victims and set dst mac address
 * @param  srcMac Mac address
 * @param  dstIp  IP address
 * @param  dstMac Mac address
 * @return        true/false
 */
bool VictimPair::isInVictimPair(string srcMac, string dstIp, uint8_t* dstMac) {
	string victim1mac = this->getVictim1()->getMacAddress();
	string victim2mac = this->getVictim2()->getMacAddress();
	// If one of victims has source mac
	// Victim 1 is source
	if (victim1mac.compare(srcMac) == 0) {
		// Check all addresses of second victim
		if (this->getVictim2()->isAddressOfHost(dstIp)) {
			// Set dst MAC address
			memcpy(dstMac, this->getVictim2()->getMacAsVector().data(), 6);
			return true;
		}
	}
	// Victim 2 is source
	else if (victim2mac.compare(srcMac) == 0) {
		// Check all addresses of second victim
		if (this->getVictim1()->isAddressOfHost(dstIp)) {
			// Set dst MAC address
			memcpy(dstMac, this->getVictim1()->getMacAsVector().data(), 6);
			return true;
		}
	}
		
	return false;
}