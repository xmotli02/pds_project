/**
 * XmlFile class used for creating xml file
 *
 * Author: Matus Motlik, xmotli02@stud.fit.vutbr.cz
 * 
 */

#include "XmlFile.h"

XmlFile::XmlFile(const string& filename) {
	this->_filename = filename;
}

void XmlFile::openFile() {
	this->_file2.open(this->_filename, ofstream::out);
	if ( !this->_file2.is_open() ) {
		cerr << "Unable to open file: " << this->_filename << endl;
	}
}

void XmlFile::closeFile() {
	this->_file2.close();
}

void XmlFile::header() {
	this->_file2 << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << endl;
}

void XmlFile::devices(){
	this->_file2 << "<devices>" << endl;
}

void XmlFile::devicesClose() {
	this->_file2 << "</devices>" << endl;
}

void XmlFile::host(string macAddress) {
	this->_file2 << "\t<host mac=\"" << macAddress << "\">" << endl;
}

void XmlFile::hostClose() {
	this->_file2 << "\t</host>" << endl;
}

void XmlFile::ipv4(string ipv4Address) {
	this->_file2 << "\t\t<ipv4>" << ipv4Address << "</ipv4>" << endl;
}

void XmlFile::ipv6(string ipv6Address) {
	this->_file2 << "\t\t<ipv6>" << ipv6Address << "</ipv6>" << endl;
}


void XmlFile::loadFile() {
	xmlInitParser();
	LIBXML_TEST_VERSION
	this->_file = xmlParseFile(this->_filename.c_str());
	XMLCheckNullPtr(this->_file, "Load file");
}

/**
 * Find or create new VictimPair with given name
 */
void XmlFile::_findOrNew(string name, hostPtr victim) {
  VictimPair newPair(name);
	vecPair::iterator it;
  it = find(_victimPairs.begin(), _victimPairs.end(), newPair);

  if (it != _victimPairs.end()) {
    it->insertVictim(victim);
  }
  else {
  	newPair.insertVictim(victim);
  	_victimPairs.push_back(newPair);
  }
}

/**
 * Get victim Mac, ipv4 and ipv6 addresses from xml file
 */
XmlFile::hostPtr XmlFile::_loadVictim(xmlNodePtr nHost) {
	hostPtr victim(new Host);
	xmlChar * mac = nullptr;
	xmlChar * ipv4 = nullptr;
	xmlChar * ipv6 = nullptr;
	xmlNodePtr nodeIp;

	// Load MAC
	mac = xmlGetProp(nHost, (const xmlChar *)"mac");
	XMLCheckNullPtr(mac, "Missing MAC address in host node");
	victim->setMacAddress((char*)mac);

	// Load IPv4 and Ipv6
	nodeIp = nHost->children;
	for( ; nodeIp != nullptr; nodeIp = nodeIp->next) {
		if (nodeIp->type == XML_ELEMENT_NODE ) {
			if (!xmlStrcmp(nodeIp->name, (const xmlChar *)"ipv4")) {
				ipv4 = xmlNodeGetContent(nodeIp);
				victim->addIpv4((char*)ipv4);
			}
			else if (!xmlStrcmp(nodeIp->name, (const xmlChar *)"ipv6")) {
				ipv6 = xmlNodeGetContent(nodeIp);
				victim->addIpv6((char*)ipv6);
			}
		}
	}
	return victim;
}

/**
 * Load xml file and return victim pairs
 */
XmlFile::vecPair XmlFile::getVectorOfVictimPairs() {
	hostPtr victim;
	xmlChar * namePtr = nullptr;
	string groupName;

	// devices node
	xmlNodePtr nDevices = xmlDocGetRootElement(this->_file);
	XMLCheckNullPtr(nDevices, "Missing devices node");

	// host node
	xmlNodePtr nHost = nDevices->children;
	XMLCheckNullPtr(nHost, "Missing hosts node");

	// Check all host nodes
	for( ; nHost != nullptr; nHost = nHost->next) {
		if (nHost->type == XML_ELEMENT_NODE ) {
			// If host node has group attribute
			namePtr = xmlGetProp(nHost, (const xmlChar *)"group");
			if (namePtr != nullptr) { 
				groupName = (char*)namePtr;
				// Insert new host to victim pair
				victim = this->_loadVictim(nHost);
				// Find or create new Victim pair
				this->_findOrNew(groupName, victim);			
			}
		}
	}

	// Cleanup
	xmlFreeDoc(this->_file);
	xmlCleanupParser();

	return _victimPairs;		
}