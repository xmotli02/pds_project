/**
 * PDS Project, 2c, Intercept
 *
 * Author: Matus Motlik, xmotli02@stud.fit.vutbr.cz
 * Description: Program prebera pakety obeti a zamenuje mac adresy, tzn
 *              uskutocnuje utork Man-In-The-Middle. Zoznam obeti nacita 
 *              z xml suboru.
 * 
 * Usage: ./pds-intercept -i interface 
 *                        -f file
 * 
 *     interface (string) - specifikuje meno rozhrania
 *     file (cesta)       - určuje subor s dvojicami obeti
 */


#include <unistd.h>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <string> 
#include <signal.h>

#include "Intercept.h"

using namespace std;

void my_handler(int s) {
  cout << "Caught signal " << s << endl;
  exit(0); 
}

int parseArgs(int argc, char **argv, Intercept *intercept) {
  int index;
  int c;
  const int PARAMS_NUMBER = 5;

  opterr = 0;

  if (argc != PARAMS_NUMBER) {
    cerr << "Wrong number of arguments!" << endl;
    return 1;
  }

  while ((c = getopt(argc, argv, "i:f:")) != -1)
    switch (c) {
      case 'i':
        intercept->setInterface(optarg);
        break;
      case 'f':
        intercept->setFileName(optarg);
        break;
      case '?':
        cerr << "Unknown option character \'" <<  (char)optopt <<"\'" << endl;
        return 1;
      default:
        return 2;
      }

  for (index = optind; index < argc; index++) {
    cerr << "Non-option argument " << argv[index] << endl;
  }

  return 0;
}

int main(int argc, char **argv) {
  struct sigaction sigIntHandler;
  sigIntHandler.sa_handler = my_handler;
  sigemptyset(&sigIntHandler.sa_mask);
  sigIntHandler.sa_flags = 0;
  sigaction(SIGINT, &sigIntHandler, NULL);

  Intercept& intercept = Intercept::getInstance();
  
  if (int errorCode = parseArgs(argc, argv, &intercept)) {
    return errorCode;
  }

  intercept.interceptOnInterface();

  return 0;
}