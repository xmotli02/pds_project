/**
 * PDS Project, 2a, Network scanner
 *
 * Author: Matus Motlik, xmotli02@stud.fit.vutbr.cz
 * Description: Program pomocou protokolov ARP a NDP oskenuje lokalnu siet
 * 
 * Usage: ./pds-scanner -i interface 
 *                      -f file
 * 
 *     interface (string) - specifikuje meno rozhrania na ktorom 
 *                          prebehne skenovanie siete
 *     file (cesta)       - určuje subor s najdenymi zariadeniami
 */


#include <unistd.h>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <string> 
#include <signal.h>

#include "Scanner.h"

using namespace std;


void my_handler(int s) {
  cout << "Caught signal " << s << endl;
  Scanner::getInstance().createXmlFile();
  exit(0); 
}


int parseArgs(int argc, char **argv, Scanner *scanner) {
  int index;
  int c;
  const int PARAMS_NUMBER = 5;

  opterr = 0;

  if (argc != PARAMS_NUMBER) {
    cerr << "Wrong number of arguments!" << endl;
    return 1;
  }

  while ((c = getopt(argc, argv, "i:f:")) != -1)
    switch (c) {
      case 'i':
        scanner->setInterface(optarg);
        break;
      case 'f':
        scanner->setFileName(optarg);
        break;
      case '?':
        cerr << "Unknown option character \'" <<  (char)optopt <<"\'" << endl;
        return 1;
      default:
        return 2;
      }

  for (index = optind; index < argc; index++) {
    cerr << "Non-option argument " << argv[index] << endl;
  }

  return 0;
}

int main(int argc, char **argv) {
  struct sigaction sigIntHandler;
  sigIntHandler.sa_handler = my_handler;
  sigemptyset(&sigIntHandler.sa_mask);
  sigIntHandler.sa_flags = 0;
  sigaction(SIGINT, &sigIntHandler, NULL);

  Scanner& scanner = Scanner::getInstance();
  
  if (int errorCode = parseArgs(argc, argv, &scanner)) {
    return errorCode;
  }

  // Scan local network to find hosts using arp an nd protocols
  scanner.scanNetwork();

  // Write hosts details to xml file
  scanner.createXmlFile();

  return 0;
}