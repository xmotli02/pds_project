/**
 * PDS Project, 2b
 * 
 * Author: Matus Motlik, xmotli02@stud.fit.vutbr.cz
 * Description: Program otravuje mapovacie cache oboch zariadeni na zvolenom 
 * 							porte, pomocou protokolov ARP a NDP. 
 * 							
 * Usage: ./pds-spoof -i interface 
 * 										-t sec 
 * 										-p protocol 
 * 										-victim1ip ipaddress 
 * 										-victim1mac macaddress 
 * 										-victim2ip ipaddress 
 * 										-victim2mac macaddress 
 *    
 *    interface (string) - specifikuje meno rozhrania
 *	  sec (celé číslo) 	 - určuje interval v milisekundách, v ktorom dochadza 
 *	  										 k opakovanému odosielaniu ARP/NDP sprav 
 *	  										 sposobujucich otravu cache
 *    protocol (string)  - urcuje pouzity protokol: "arp" alebo "ndp"
 *    ipaddress 				 - je buď IPv4 (v dotted-decimal formate), alebo
 *    										 IPv6 adresa obete (vo formáte uvedenom v RFC5952)
 *    macaddress 				 - je MAC adresa obete.
 */

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <string> 
#include <memory>

#include <unistd.h>
#include <signal.h>
#include <getopt.h>

#include "Spoof.h"
#include "Host.h"

using namespace std;

void my_handler(int s) {
  cout << "Caught signal " << s << endl;
  Spoof::getInstance().resetVictimsCaches();
  exit(0); 
}

int parseArgs(int argc, char **argv, Spoof *spoof) {
  int index;
  int c;
  int option_index = 0;
  const int PARAMS_NUMBER = 15;

  shared_ptr<Host> victim1(new Host);;
  shared_ptr<Host> victim2(new Host);;

  static struct option long_options[] = {
		{"interface",  1, 0, 'i'},
		{"time",  		 1, 0, 't'},
		{"protocol",   1, 0, 'p'},
		{"victim1ip",  1, 0, 'a'},
		{"victim1mac", 1, 0, 'b'},
		{"victim2ip",  1, 0, 'c'},
		{"victim2mac", 1, 0, 'd'},
		{0, 0, 0, 0}
	};

	if (argc != PARAMS_NUMBER) {
    cerr << "Wrong number of arguments!" << endl;
    return 1;
  }
  
  while ((c = getopt_long_only(argc, argv, "i:t:p:a:b:c:d:", 
  									 			long_options, &option_index)) != -1) {  

		switch (c) {
			case 0:
				if (long_options[option_index].flag != 0)
					break;
				cout << "option " << long_options[option_index].name;
				if (optarg)
					cout << " with arg " << optarg;
				cout << endl;
				break;
			case 'i':
				spoof->setInterface(optarg);
				break;
			case 't':
				spoof->setTimeout(atoi(optarg));
				break;
			case 'p':
				spoof->setProtocol(optarg);
				break;
			case 'a':
				victim1->setIpAddress(optarg);
				break;
			case 'b':
				victim1->setMacAddress(optarg);
				break;
			case 'c':
				victim2->setIpAddress(optarg);
				break;
			case 'd':
				victim2->setMacAddress(optarg);
				break;
			case '?':
				break;

			default:
				abort();
		}

		option_index = 0;
	}

	// Save victims
	spoof->setVictim1(victim1);
	spoof->setVictim2(victim2);

  for (index = optind; index < argc; index++) {
    cerr << "Non-option argument " << argv[index] << endl;
  }

  return 0;
}

int main(int argc, char **argv) {
  struct sigaction sigIntHandler;
  sigIntHandler.sa_handler = my_handler;
  sigemptyset(&sigIntHandler.sa_mask);
  sigIntHandler.sa_flags = 0;
  sigaction(SIGINT, &sigIntHandler, NULL);

  Spoof& spoof = Spoof::getInstance();

  if (int errorCode = parseArgs(argc, argv, &spoof)) {
    return errorCode;
  }

	cout << "Interface: " << spoof.getInterface()->getName() << "; "
			 << "Timeout: " << spoof.getTimeout() << "; "
			 << "Protocol: " << (int)spoof.getProtocol() << "; "
			 << "Victim1: " << spoof.getVictim1()->getIpAddress() << ", "
			 << spoof.getVictim1()->getMacAddress() << "; "
			 << "Victim1: " << spoof.getVictim2()->getIpAddress() << ", "
			 << spoof.getVictim2()->getMacAddress() << endl;

  // Spoof victims
  spoof.spoofVictims();

  return 0;
}